# biblioteca-api
Proyecto Java Intermedio

Nombre del Proyecto: Sistema de Gestión de Biblioteca (Backend)

Descripción
Desarrolla un sistema de gestión de biblioteca (solo el backend) que permita a los bibliotecarios y a los usuarios administrar y buscar libros, llevar un registro de préstamos y devoluciones, y generar reportes basados en la información de la biblioteca.

Requisitos Funcionales:
Gestión de Libros
Agregar, editar y eliminar libros con detalles como título, autor, año de publicación, género, etc.
Buscar libros por título, autor o género.
Gestión de Préstamos
Lleva un registro de los libros prestados, incluyendo la fecha de inicio y la fecha de vencimiento.
Devoluciones de Libros
Registra la devolución de libros prestados.
Calcula multas por devoluciones atrasadas.
Generación de Reportes
Crea un informe de los libros más populares en un periodo de días.
Búsqueda con Named Queries
Implementa consultas con Named Queries para buscar libros por criterios específicos, como género o año de publicación.

Requisitos Técnicos:
Implementa la lógica de negocio utilizando Java.
Utiliza una base de datos relacional (por ejemplo, MySQL o PostgreSQL) para almacenar la información de libros, préstamos, etc.
Aplica los conceptos de JPA y Spring Data para mapear las entidades a la base de datos.
Crea servicios para gestionar libros, préstamos y devoluciones.
Implementa funcionalidades de búsqueda con Named Queries.
Genera informes utilizando Java Streams para procesar y resumir datos.
Realiza pruebas unitarias y de integración para garantizar la calidad del código.
Utiliza un sistema de construcción como Maven o Gradle.
Despliega la aplicación en un servidor web o en el entorno de Spring Boot.
 